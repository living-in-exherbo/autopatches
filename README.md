paludis-autopatches
===================

My set of patches to apply beside exheres do.
To do so, I use my [autopatch](https://gitlab.exherbo.org/living-in-exherbo/paludis-hooks)
hook to avoid re-writing exheres just to test some trivial patch.
Default path to clone is `/var/db/paludis/autopatches`, but one can override
it in autopatch config (`/etc/paludis/hooks/config/auto-patch.conf`).
